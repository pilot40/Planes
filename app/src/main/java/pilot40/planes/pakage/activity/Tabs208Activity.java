package pilot40.planes.pakage.activity;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

import pilot40.planes.pakage.R;
import pilot40.planes.pakage.fragments.FuelFragment208;
import pilot40.planes.pakage.fragments.FragmentVeter208;
import pilot40.planes.pakage.fragments.Fragment67422_pass_208;
import pilot40.planes.pakage.fragments.Fragment67422_sz1_208;
import pilot40.planes.pakage.fragments.Fragment67422_sz2_208;
import pilot40.planes.pakage.fragments.Fragment67450_pass_208;
import pilot40.planes.pakage.fragments.Fragment67450_sz1_208;
import pilot40.planes.pakage.fragments.Fragment67450_sz2_208;
import pilot40.planes.pakage.fragments.Fragment67450_per_208;
import pilot40.planes.pakage.fragments.Fragment67450_rab_208;
import pilot40.planes.pakage.fragments.Fragment67720_pass_208;
import pilot40.planes.pakage.fragments.Fragment67720_sz1_208;
import pilot40.planes.pakage.fragments.Fragment67720_sz2_208;

public class Tabs208Activity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_208_tabs);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FuelFragment208(), "Топливо");
        adapter.addFragment(new FragmentVeter208(), "Ветер");
        adapter.addFragment(new Fragment67422_pass_208(), "67422-пасс");
        adapter.addFragment(new Fragment67422_sz1_208(), "67422-сз1");
        adapter.addFragment(new Fragment67422_sz2_208(), "67422-сз2");
        adapter.addFragment(new Fragment67450_pass_208(), "67450-пасс");
        adapter.addFragment(new Fragment67450_sz1_208(), "67450-сз1");
        adapter.addFragment(new Fragment67450_sz2_208(), "67450-сз2");
        adapter.addFragment(new Fragment67450_per_208(), "67450-пер");
        adapter.addFragment(new Fragment67450_rab_208(), "67450-раб");
        adapter.addFragment(new Fragment67720_pass_208(), "67720-пасс");
        adapter.addFragment(new Fragment67720_sz1_208(), "67720-сз1");
        adapter.addFragment(new Fragment67720_sz2_208(), "67720-сз2");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
