package pilot40.planes.pakage.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import pilot40.planes.pakage.R;

/**
 * Самый главный класс всего приложения
 * @author Михаил Зуев
 * @version 1.3.5
 */
public class MainActivity extends AppCompatActivity{

    private Toolbar toolbar;
    private Button btn208Tabs, btnDHCTabs, btn206Tabs, btnPilatusTabs, btnPogodaTabs, btnPerevodTabs;

    /**
     * Создаются все причиндалы и определяются кнопки на главном экране
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btn208Tabs = (Button) findViewById(R.id.btn208Tabs);
        btnDHCTabs = (Button) findViewById(R.id.btnDHCTabs);
        btn206Tabs = (Button) findViewById(R.id.btn206Tabs);
        btnPilatusTabs = (Button) findViewById(R.id.btnPilatusTabs);
        btnPogodaTabs = (Button) findViewById(R.id.btnPogodaTabs);
        btnPerevodTabs = (Button) findViewById(R.id.btnPerevodTabs);

        btn208Tabs.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Tabs208Activity.class));
            }
        });

        btnDHCTabs.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, TabsDHCActivity.class));
            }
        });

        btn206Tabs.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Tabs206Activity.class));
            }
        });

        btnPilatusTabs.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, TabsPilatusActivity.class));
            }
        });

        btnPogodaTabs.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, TabsPogodaActivity.class));
            }
        });

        btnPerevodTabs.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, TabsPerevodActivity.class));
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // получим идентификатор выбранного пункта меню
        int id = item.getItemId();

        // Операции для выбранного пункта меню
        switch (id) {
            case R.id.action_settings:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
            return true;
            case R.id.action_about:
                //Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();

                Toast.makeText(this,"Самолёты Аэрогео\nООО Аэрогео\nВерсия 1.3.5\nАвтор: Зуев М.Г.\npilot40@gmail.com\nЛицензия: GPL v.3",Toast.LENGTH_LONG).show();
                return true;
            case R.id.action_help:
                //Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                Toast.makeText(this,"П-пассажирский вариант\nС1-с/з одни носилки\nС2-с/з двое носилок\nНОС - носилки\nПЕР - перегон\nРАБ - работа\nB4 и B6 багаж в салоне\nУгол ветра:\n+ справа\n- слева",Toast.LENGTH_LONG).show();
                return true;
            case R.id.action_exit:
                //Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                //this.finish();
                android.os.Process.killProcess(android.os.Process.myPid());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
