package pilot40.planes.pakage.fragments;

import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import pilot40.planes.pakage.R;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.BLUE;
import static android.graphics.Color.RED;


public class Fragment67720_pass_208 extends Fragment implements View.OnClickListener {


    public EditText edGob, edTopl67720_sz1;
    private TextView r1_67720pass, r2_67720pass, r3_67720pass, bagA67720pass, bagB67720pass, bagC67720pass, bagD67720pass, b4_67720pass, b6_67720pass;
    private EditText edTopl67720pass, edA67720pass, edB67720pass, edC67720pass, edD67720pass, edPred67720pass, edVes67720pass, edTks67720pass,
            edEk67720pass, edR1_67720pass, edR2_67720pass, edR3_67720pass, edB4_67720pass, edB6_67720pass, edCentr67720pass, edCG67720pass;
    private CheckBox cbLeto_67720_pass;
    private FloatingActionButton fb_67720_pass;


    public Fragment67720_pass_208() {
        // Required empty public constructor

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Check whether we're recreating a previously destroyed instance
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_67720_pass, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();


        Button btRassch67720pass = (Button) getActivity().findViewById(R.id.btRassch67720pass);

        cbLeto_67720_pass = (CheckBox) getActivity().findViewById(R.id.cbLeto_67720_pass);

        edTopl67720pass = (EditText) getActivity().findViewById(R.id.edTopl67720pass);
        edA67720pass = (EditText) getActivity().findViewById(R.id.edA67720pass);
        edB67720pass = (EditText) getActivity().findViewById(R.id.edB67720pass);
        edC67720pass = (EditText) getActivity().findViewById(R.id.edC67720pass);
        edD67720pass = (EditText) getActivity().findViewById(R.id.edD67720pass);
        edTks67720pass = (EditText) getActivity().findViewById(R.id.edTks67720pass);
        edPred67720pass = (EditText) getActivity().findViewById(R.id.edPred67720pass);
        edVes67720pass = (EditText) getActivity().findViewById(R.id.edVes67720pass);
        edEk67720pass = (EditText) getActivity().findViewById(R.id.edEk67720pass);
        edR1_67720pass = (EditText) getActivity().findViewById(R.id.edR1_67720pass);
        edR2_67720pass = (EditText) getActivity().findViewById(R.id.edR2_67720pass);
        edR3_67720pass = (EditText) getActivity().findViewById(R.id.edR3_67720pass);
        edB4_67720pass = (EditText) getActivity().findViewById(R.id.edB4_67720pass);
        edB6_67720pass = (EditText) getActivity().findViewById(R.id.edB6_67720pass);
        edCentr67720pass = (EditText) getActivity().findViewById(R.id.edCentr67720pass);
        edCG67720pass = (EditText) getActivity().findViewById(R.id.edCG67720pass);
        r1_67720pass = (TextView) getActivity().findViewById(R.id.r1_67720pass);
        r2_67720pass = (TextView) getActivity().findViewById(R.id.r2_67720pass);
        r3_67720pass = (TextView) getActivity().findViewById(R.id.r3_67720pass);
        bagA67720pass = (TextView) getActivity().findViewById(R.id.bagA67720pass);
        bagB67720pass = (TextView) getActivity().findViewById(R.id.bagB67720pass);
        bagC67720pass = (TextView) getActivity().findViewById(R.id.bagC67720pass);
        bagD67720pass = (TextView) getActivity().findViewById(R.id.bagD67720pass);
        b4_67720pass = (TextView) getActivity().findViewById(R.id.b4_67720pass);
        b6_67720pass = (TextView) getActivity().findViewById(R.id.b6_67720pass);


        fb_67720_pass = (FloatingActionButton) getActivity().findViewById(R.id.fb_67720_pass);
        fb_67720_pass.show();

        fb_67720_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edR1_67720pass.setText("0");
                edR2_67720pass.setText("0");
                edR3_67720pass.setText("0");
                edA67720pass.setText("0");
                edB67720pass.setText("0");
                edC67720pass.setText("0");
                edD67720pass.setText("0");
                edB4_67720pass.setText("0");
                edB6_67720pass.setText("0");
                edCentr67720pass.setText("0");
                edCG67720pass.setText("0");
                edPred67720pass.setText("0");
                edVes67720pass.setText("0");
            }
        });

        try {
            edGob = (EditText) getActivity().findViewById(R.id.edGob);
            edTopl67720pass.setText(String.valueOf(edGob.getText().toString()));
        } catch (NullPointerException e) {
        }
        try {
            edTopl67720_sz1 = (EditText) getActivity().findViewById(R.id.edTopl67720_sz1);
            edTopl67720pass.setText(String.valueOf(edTopl67720_sz1.getText().toString()));
        } catch (NullPointerException e) {
        }

        if ((edTopl67720pass.getText().toString().equals(""))) {
            Toast toast2 = Toast.makeText(getActivity(), "Вес топлива не введён", Toast.LENGTH_LONG);
            toast2.show();
            edTopl67720pass.setText("0");
        } else if (edR1_67720pass.getText().toString().equals("")) {
            edR1_67720pass.setText("0");
        } else if (edR2_67720pass.getText().toString().equals("")) {
            edR2_67720pass.setText("0");
        } else if (edR3_67720pass.getText().toString().equals("")) {
            edR3_67720pass.setText("0");
        }

        r1_67720pass.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR1_67720pass.getText().toString());
                if (cbLeto_67720_pass.isChecked()) {
                    if (c < 225) {
                        edR1_67720pass.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }

                } else {
                    if (c < 240) {
                        edR1_67720pass.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        r2_67720pass.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR2_67720pass.getText().toString());
                if (cbLeto_67720_pass.isChecked()) {
                    if (c < 225) {
                        edR2_67720pass.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Второй ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                } else {
                    if (c < 240) {
                        edR2_67720pass.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Второй ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        r3_67720pass.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR3_67720pass.getText().toString());
                if (cbLeto_67720_pass.isChecked()) {
                    if (c < 225) {
                        edR3_67720pass.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                } else {
                    if (c < 240) {
                        edR3_67720pass.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        bagA67720pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edA67720pass.getText().toString());
                if (c <= 104) {
                    edA67720pass.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Багаж А перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }

            }
        });

        bagB67720pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edB67720pass.getText().toString());
                if (c <= 141) {
                    edB67720pass.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Багаж B перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        bagC67720pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edC67720pass.getText().toString());
                if (c <= 123) {
                    edC67720pass.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Багаж C перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        bagD67720pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edD67720pass.getText().toString());
                if (c <= 127) {
                    edD67720pass.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Багаж D перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        b4_67720pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edB4_67720pass.getText().toString());
                if (c <= 154) {
                    edB4_67720pass.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Зона B4 перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        b6_67720pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edB6_67720pass.getText().toString());
                if (c <= 154) {
                    edB6_67720pass.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Зона B6 перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        btRassch67720pass.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if ((edTopl67720pass.getText().toString().equals(""))) {
            Toast toast2 = Toast.makeText(getActivity(), "Вес топлива не введён", Toast.LENGTH_LONG);
            toast2.show();
            edTopl67720pass.setText("0");
        }
        if ((edTks67720pass.getText().toString().equals(""))) {
            edTks67720pass.setText("0");
        }
        if ((edA67720pass.getText().toString().equals(""))) {
            edA67720pass.setText("0");
        }
        if ((edB67720pass.getText().toString().equals(""))) {
            edB67720pass.setText("0");
        }
        if ((edC67720pass.getText().toString().equals(""))) {
            edC67720pass.setText("0");
        }
        if ((edD67720pass.getText().toString().equals(""))) {
            edD67720pass.setText("0");
        }
        if ((edB4_67720pass.getText().toString().equals(""))) {
            edB4_67720pass.setText("0");
        }
        if ((edB6_67720pass.getText().toString().equals(""))) {
            edB6_67720pass.setText("0");
        }
        if ((edR1_67720pass.getText().toString().equals(""))) {
            edR1_67720pass.setText("0");
        }
        if ((edR2_67720pass.getText().toString().equals(""))) {
            edR2_67720pass.setText("0");
        }
        if ((edR3_67720pass.getText().toString().equals(""))) {
            edR3_67720pass.setText("0");
        }

        float gPust = 2389;//вес пустого кг
        int gSl = 38;
        int gZem = 16;
        int gFuel = Integer.parseInt(edTopl67720pass.getText().toString());
        int vEk = (Integer.parseInt(edEk67720pass.getText().toString())) * 80;
        float tks1 = (Float.parseFloat(edTks67720pass.getText().toString())) * 4.1731f;
        float tks = (Float.parseFloat(edTks67720pass.getText().toString()));
        float r1 = (Float.parseFloat(edR1_67720pass.getText().toString()));
        float r2 = (Float.parseFloat(edR2_67720pass.getText().toString()));
        float r3 = (Float.parseFloat(edR3_67720pass.getText().toString()));
        float a = (Float.parseFloat(edA67720pass.getText().toString()));
        float b = (Float.parseFloat(edB67720pass.getText().toString()));
        float c = (Float.parseFloat(edC67720pass.getText().toString()));
        float d = (Float.parseFloat(edD67720pass.getText().toString()));
        float b4 = (Float.parseFloat(edB4_67720pass.getText().toString()));
        float b6 = (Float.parseFloat(edB6_67720pass.getText().toString()));

        if ((edR1_67720pass.getText().toString().equals(""))) {
            edR1_67720pass.setText("0");
        } else if ((edR2_67720pass.getText().toString().equals(""))) {
            edR2_67720pass.setText("0");
        } else if ((edR3_67720pass.getText().toString().equals(""))) {
            edR3_67720pass.setText("0");
        }

        if (cbLeto_67720_pass.isChecked()) {
            if (r1 > 225) {
                Toast toast4 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                toast4.show();
            } else if (r2 > 225) {
                Toast toast5 = Toast.makeText(getActivity(), "Второй ряд перебор!", Toast.LENGTH_LONG);
                toast5.show();
            } else if (r3 > 225) {
                Toast toast6 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                toast6.show();
            }
        } else {
            if (r1 > 240) {
                Toast toast7 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                toast7.show();
            } else if (r2 > 240) {
                Toast toast8 = Toast.makeText(getActivity(), "Второй ряд перебор!", Toast.LENGTH_LONG);
                toast8.show();
            } else if (r3 > 240) {
                Toast toast9 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                toast9.show();
            }
        }

        float gVzl = gPust + (float) gSl + (float) gFuel + tks1 + (float) vEk + r1 + r2 + r3 + a + b + c + d + b4 + b6 - (float) gZem;
        edVes67720pass.setText(String.valueOf((int) gVzl));
        edVes67720pass.setTextColor(BLACK);
        float gPred = 3969 - (gPust + (float) gSl + (float) gFuel + tks1 + (float) vEk - (float) gZem);
        edPred67720pass.setTextColor(BLUE);
        edPred67720pass.setText(String.valueOf((int) gPred));
        if (gVzl > 3969) {
            edVes67720pass.setTextColor(RED);
            Toast toast10 = Toast.makeText(getActivity(), "Превышение\nвзлётного\nвеса!!!", Toast.LENGTH_LONG);
            toast10.show();
        }
        /////Далее центровка
        float cG = (((1023.01f + 22.35f + (((float) gFuel / 0.4536f) / 4.92f)
                + 23.90f + 23.90f + (((r1 / 0.4536f) * 192.6f) / 1000f)
                + (((r2 / 0.4536f) * 229.2f) / 1000f) + (((r3 / 0.4536f) * 265.8f) / 1000f)
                + (((a / 0.4536f) * 132.4f) / 1000f) + (((b / 0.4536f) * 182.1f) / 1000f)
                + (((c / 0.4536f) * 233.4f) / 1000f) + (((d / 0.4536f) * 287.6f) / 1000f)
                + (((b4 / 0.4536f) * 294.5f) / 1000f) + (((b6 / 0.4536f) * 344f) / 1000f)
                + (((tks * 9.2f) * 195.77f) / 1000f) - 7.11f) / (gVzl / 0.4536f)) * 1000f);
        float cEntr = (cG - 177.57f) / 0.664f;
        String formattedDouble = new DecimalFormat("#0.00").format(cG);
        edCG67720pass.setText(formattedDouble);//вывод плеча CG location(для графика)
        //edCG67720pass.setText(String.valueOf(Math.round(cG)));//вывод плеча CG location(для графика)
        //edCG67720pass.setText(String.valueOf(cG));//вывод плеча CG location(для графика)
        //edCG67720pass.setText(String.valueOf(Math.floor(cG)));//вывод плеча CG location(для графика)
        edCentr67720pass.setTextColor(BLACK);
        edCentr67720pass.setText(String.valueOf((Math.floor(cEntr * 100f + 0.5f) / 100f)));

        if (cEntr > 40.33) {
            edCentr67720pass.setTextColor(RED);
            Toast toast10 = Toast.makeText(getActivity(), "Предельно\nзадняя\nцентровка!!", Toast.LENGTH_LONG);
            toast10.show();
        }
        if (gVzl <= 3629) {
            float сMin = ((gVzl - 2327.6885245902f) / 54.6769527483f);
            if (cEntr < сMin) {
                edCentr67720pass.setTextColor(RED);
                Toast toast10 = Toast.makeText(getActivity(), "Предельно\nпередняя\nцентровка!!", Toast.LENGTH_LONG);
                toast10.show();
            }
        } else if ((gVzl > 3629) & (gVzl <= 3969)) {
            float cMin = ((gVzl - 2698.8850574713f) / 39.0804597701f);
            if (cEntr < cMin) {
                edCentr67720pass.setTextColor(RED);
                Toast toast10 = Toast.makeText(getActivity(), "Предельно\nпередняя\nцентровка!!", Toast.LENGTH_LONG);
                toast10.show();
            }
        }
    }

}
//алес :):)

