package pilot40.planes.pakage.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pilot40.planes.pakage.R;

public class FuelFragmentDHC extends Fragment implements View.OnClickListener {


    public  EditText edRasst, edZapas, edSpeed, edRash, edGpol, edGzap, edTime, edGob;




    public FuelFragmentDHC() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_fuel_dhc, container, false);
    }



    @Override
    public void onStart() {
        super.onStart();
        Button btn = (Button) getActivity().findViewById(R.id.button1);

        edRasst = (EditText) getActivity().findViewById(R.id.edRasst);
        edZapas = (EditText) getActivity().findViewById(R.id.edZapas);
        edSpeed = (EditText) getActivity().findViewById(R.id.edSpeed);
        edRash = (EditText) getActivity().findViewById(R.id.edRash);
        edGob = (EditText) getActivity().findViewById(R.id.edGob);
        edGpol = (EditText) getActivity().findViewById(R.id.edGpol);
        edGzap = (EditText) getActivity().findViewById(R.id.edGzap);
        edTime = (EditText) getActivity().findViewById(R.id.edTime);

        btn.setOnClickListener(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        setRetainInstance(true);
    }

    @Override
    public void onClick(View view) {

        if (((edRasst.getText().toString().equals(""))) | (edZapas.getText().toString().equals(""))) {
            Toast toast = Toast.makeText(getActivity(), "Введите значения!", Toast.LENGTH_LONG);
            toast.show();
        } else {
            int V = Integer.parseInt(edSpeed.getText().toString());
            int Q = Integer.parseInt(edRash.getText().toString());
            int S = Integer.parseInt(edRasst.getText().toString());
            int Salt = Integer.parseInt(edZapas.getText().toString());


            float Tzap = (float) Salt / V;//Время полета до запасного
            float Tp = (float) S / V; //Время полета до ап
            int hours = (int) Tp;
            int minutes = (int) (60 * (Tp - hours));
            double Gpol = (Tp * Q * 0.03) + (Tp * Q);
            double Gzap = (Tzap * Q) + (Q/2);
            double Gf = Gpol + Gzap;
            edGpol.setText(String.valueOf((int) Gpol));
            edGzap.setText(String.valueOf((int) Gzap));
            edGob.setText(String.valueOf((int) Gf));
            ///надо как то передать это в расчёт веса. ПЕРЕДАЛ!!!!!!

            if ((hours < 10) & (minutes < 10)) {
                edTime.setText("0" + hours + ":0" + minutes);
            } else if ((hours < 10) & (minutes >= 10)) {
                edTime.setText("0" + hours + ":" + minutes);
            }
        }

    }

}


