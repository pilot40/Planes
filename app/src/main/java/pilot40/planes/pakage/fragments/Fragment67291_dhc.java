package pilot40.planes.pakage.fragments;

import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import pilot40.planes.pakage.R;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.BLUE;
import static android.graphics.Color.RED;
import static java.lang.Math.round;


public class Fragment67291_dhc extends Fragment implements View.OnClickListener {

    public EditText edGob;
    private EditText edtopl67291_p_dhc, edtopl67291_z_dhc, edtopl67291_ob_dhc, edVes67291_kg_dhc, edVes67291_ft_dhc;
    private CheckBox cbLeto_67291_dhc, cbPos_67291_dhc;
    private TextView r1_67291, r2_67291, r3_67291, r4_67291, r5_67291, r6_67291, r7_67291, cf_67291, af_67291;
    private EditText edR1_67291, edR2_67291, edR3_67291, edR4_67291, edR5_67291, edR6_67291, edR7_67291;
    private EditText edPred67291_dhc, edCentr67291_dhc, edCG67291, edCF_67291, edAF_67291;
    private FloatingActionButton fb_67291;


    public Fragment67291_dhc() {
        // Required empty public constructor

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Check whether we're recreating a previously destroyed instance
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_67291_dhc, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();


        Button btRassch67291 = (Button) getActivity().findViewById(R.id.btRassch67291);

        cbLeto_67291_dhc = (CheckBox) getActivity().findViewById(R.id.cbLeto_67291_dhc);
        cbPos_67291_dhc = (CheckBox) getActivity().findViewById(R.id.cbPos_67291_dhc );

        edtopl67291_p_dhc = (EditText) getActivity().findViewById(R.id.edtopl67291_p_dhc);
        edtopl67291_z_dhc = (EditText) getActivity().findViewById(R.id.edtopl67291_z_dhc);
        edtopl67291_ob_dhc = (EditText) getActivity().findViewById(R.id.edtopl67291_ob_dhc);
        edVes67291_kg_dhc = (EditText) getActivity().findViewById(R.id.edVes67291_kg_dhc);
        edVes67291_ft_dhc = (EditText) getActivity().findViewById(R.id.edVes67291_ft_dhc);
        edPred67291_dhc = (EditText) getActivity().findViewById(R.id.edPred67291_dhc);
        edCentr67291_dhc = (EditText) getActivity().findViewById(R.id.edCentr67291_dhc);
        edCG67291 = (EditText) getActivity().findViewById(R.id.edCG67291);
        edR1_67291 = (EditText) getActivity().findViewById(R.id.edR1_67291);
        edR2_67291 = (EditText) getActivity().findViewById(R.id.edR2_67291);
        edR3_67291 = (EditText) getActivity().findViewById(R.id.edR3_67291);
        edR4_67291 = (EditText) getActivity().findViewById(R.id.edR4_67291);
        edR5_67291 = (EditText) getActivity().findViewById(R.id.edR5_67291);
        edR6_67291 = (EditText) getActivity().findViewById(R.id.edR6_67291);
        edR7_67291 = (EditText) getActivity().findViewById(R.id.edR7_67291);
        edCF_67291 = (EditText) getActivity().findViewById(R.id.edCF_67291);
        edAF_67291 = (EditText) getActivity().findViewById(R.id.edAF_67291);

        r1_67291 = (TextView) getActivity().findViewById(R.id.r1_67291);
        r2_67291 = (TextView) getActivity().findViewById(R.id.r2_67291);
        r3_67291 = (TextView) getActivity().findViewById(R.id.r3_67291);
        r4_67291 = (TextView) getActivity().findViewById(R.id.r4_67291);
        r5_67291 = (TextView) getActivity().findViewById(R.id.r5_67291);
        r6_67291 = (TextView) getActivity().findViewById(R.id.r6_67291);
        r7_67291 = (TextView) getActivity().findViewById(R.id.r7_67291);
        cf_67291 = (TextView) getActivity().findViewById(R.id.cf_67291);
        af_67291 = (TextView) getActivity().findViewById(R.id.af_67291);

        fb_67291 = (FloatingActionButton) getActivity().findViewById(R.id.fb_67291);
        fb_67291.show();

        fb_67291.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edR1_67291.setText("0");
                edR2_67291.setText("0");
                edR3_67291.setText("0");
                edR4_67291.setText("0");
                edR5_67291.setText("0");
                edR6_67291.setText("0");
                edR7_67291.setText("0");
                edCF_67291.setText("45");
                edAF_67291.setText("0");
                edVes67291_kg_dhc.setText("0");
                edVes67291_ft_dhc.setText("0");
                edPred67291_dhc.setText("0");
                edCentr67291_dhc.setText("0");
                edCG67291.setText("0");
            }
        });

        try {
            edGob = (EditText) getActivity().findViewById(R.id.edGob);
            edtopl67291_ob_dhc.setText(String.valueOf(edGob.getText().toString()));
            if ((edtopl67291_ob_dhc.getText().toString().equals(""))) {
                Toast toast2 = Toast.makeText(getActivity(), "Вес общего топлива не введён", Toast.LENGTH_LONG);
                toast2.show();
                edtopl67291_ob_dhc.setText("0");
            } else if ((edtopl67291_p_dhc.getText().toString().equals(""))) {
                Toast toast2 = Toast.makeText(getActivity(), "Вес топлива FWD не введён", Toast.LENGTH_LONG);
                toast2.show();
                edtopl67291_p_dhc.setText("0");
            } else if ((edtopl67291_z_dhc.getText().toString().equals(""))) {
                Toast toast2 = Toast.makeText(getActivity(), "Вес топлива AFT не введён", Toast.LENGTH_LONG);
                toast2.show();
                edtopl67291_z_dhc.setText("0");
            }
            int p = Integer.parseInt(edtopl67291_ob_dhc.getText().toString());
            if (p !=0){
                edtopl67291_p_dhc.setText(String.valueOf(p/2));
                edtopl67291_z_dhc.setText(String.valueOf(p/2));
            } else {
                edtopl67291_p_dhc.setText("0");
                edtopl67291_z_dhc.setText("0");
            }

        } catch (NullPointerException e) {
        }

        if ((edtopl67291_ob_dhc.getText().toString().equals(""))) {
            Toast toast2 = Toast.makeText(getActivity(), "Вес общего топлива не введён", Toast.LENGTH_LONG);
            toast2.show();
            edtopl67291_ob_dhc.setText("0");
        } else if ((edtopl67291_p_dhc.getText().toString().equals(""))) {
            Toast toast2 = Toast.makeText(getActivity(), "Вес FWD топлива не введён", Toast.LENGTH_LONG);
            toast2.show();
            edtopl67291_p_dhc.setText("0");
        } else if ((edtopl67291_z_dhc.getText().toString().equals(""))) {
            Toast toast2 = Toast.makeText(getActivity(), "Вес AFT топлива не введён", Toast.LENGTH_LONG);
            toast2.show();
            edtopl67291_z_dhc.setText("0");
        } else if (edR1_67291.getText().toString().equals("")) {
            edR1_67291.setText("0");
        } else if (edR2_67291.getText().toString().equals("")) {
            edR2_67291.setText("0");
        } else if (edR3_67291.getText().toString().equals("")) {
            edR3_67291.setText("0");
        } else if (edR4_67291.getText().toString().equals("")) {
            edR4_67291.setText("0");
        } else if (edR5_67291.getText().toString().equals("")) {
            edR5_67291.setText("0");
        } else if (edR6_67291.getText().toString().equals("")) {
            edR6_67291.setText("0");
        } else if (edR7_67291.getText().toString().equals("")) {
            edR7_67291.setText("0");
        }

        r1_67291.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR1_67291.getText().toString());
                if (cbLeto_67291_dhc.isChecked()) {
                    if (c < 225) {
                        edR1_67291.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }

                } else {
                    if (c < 240) {
                        edR1_67291.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        r2_67291.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR2_67291.getText().toString());
                if (cbLeto_67291_dhc.isChecked()) {
                    if (c < 225) {
                        edR2_67291.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Второй ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                } else {
                    if (c < 240) {
                        edR2_67291.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Второй ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        r3_67291.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR3_67291.getText().toString());
                if (cbLeto_67291_dhc.isChecked()) {
                    if (c < 225) {
                        edR3_67291.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                } else {
                    if (c < 240) {
                        edR3_67291.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        r4_67291.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR4_67291.getText().toString());
                if (cbLeto_67291_dhc.isChecked()) {
                    if (c < 225) {
                        edR4_67291.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Четвёртый ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                } else {
                    if (c < 240) {
                        edR4_67291.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Четвёртый ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        r5_67291.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR5_67291.getText().toString());
                if (cbLeto_67291_dhc.isChecked()) {
                    if (c < 225) {
                        edR5_67291.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Пятый ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                } else {
                    if (c < 240) {
                        edR5_67291.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Пятый ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        r6_67291.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR6_67291.getText().toString());
                if (cbLeto_67291_dhc.isChecked()) {
                    if (c < 75) {
                        edR6_67291.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Шестой ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                } else {
                    if (c < 80) {
                        edR6_67291.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Шестой ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        r7_67291.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR7_67291.getText().toString());
                if (cbLeto_67291_dhc.isChecked()) {
                    if (c < 225) {
                        edR7_67291.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Седьмой ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                } else {
                    if (c < 240) {
                        edR7_67291.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Седьмой ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        cf_67291.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edCF_67291.getText().toString());
                if (c <= 130) {
                    edCF_67291.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Передний багажник перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }

            }
        });

        af_67291.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edAF_67291.getText().toString());
                if (c <= 227) {
                    edAF_67291.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Задний багажник  перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });


        btRassch67291.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if ((edtopl67291_p_dhc.getText().toString().equals(""))) {
            Toast toast2 = Toast.makeText(getActivity(), "Вес общего топлива не введён", Toast.LENGTH_LONG);
            toast2.show();
            edtopl67291_p_dhc.setText("0");
        }
        if ((edtopl67291_z_dhc.getText().toString().equals(""))) {
            Toast toast22 = Toast.makeText(getActivity(), "Вес FWD топлива не введён", Toast.LENGTH_LONG);
            toast22.show();
            edtopl67291_z_dhc.setText("0");
        }
        if ((edtopl67291_ob_dhc.getText().toString().equals(""))) {
            Toast toast222 = Toast.makeText(getActivity(), "Вес AFT топлива не введён", Toast.LENGTH_LONG);
            toast222.show();
            edtopl67291_ob_dhc.setText("0");
            if ((edR1_67291.getText().toString().equals(""))) {
                edR1_67291.setText("0");
            }
            if ((edR2_67291.getText().toString().equals(""))) {
                edR2_67291.setText("0");
            }
            if ((edR3_67291.getText().toString().equals(""))) {
                edR3_67291.setText("0");
            }
            if ((edR4_67291.getText().toString().equals(""))) {
                edR4_67291.setText("0");
            }
            if ((edR5_67291.getText().toString().equals(""))) {
                edR5_67291.setText("0");
            }
            if ((edR6_67291.getText().toString().equals(""))) {
                edR6_67291.setText("0");
            }
            if ((edR7_67291.getText().toString().equals(""))) {
                edR7_67291.setText("0");
            }
            if ((edCF_67291.getText().toString().equals(""))) {
                edCF_67291.setText("45");
            }
            if ((edAF_67291.getText().toString().equals(""))) {
                edAF_67291.setText("0");
            }
        }

        float gPust = 3496;//вес пустого кг плечо 213.99
        int gZem = 45;
        int gEk = 160;
        int gFuel_p = Integer.parseInt(edtopl67291_p_dhc.getText().toString());
        int gFuel_z = Integer.parseInt(edtopl67291_z_dhc.getText().toString());

        float r1 = (Float.parseFloat(edR1_67291.getText().toString()));
        float r2 = (Float.parseFloat(edR2_67291.getText().toString()));
        float r3 = (Float.parseFloat(edR3_67291.getText().toString()));
        float r4 = (Float.parseFloat(edR4_67291.getText().toString()));
        float r5 = (Float.parseFloat(edR5_67291.getText().toString()));
        float r6 = (Float.parseFloat(edR6_67291.getText().toString()));
        float r7 = (Float.parseFloat(edR7_67291.getText().toString()));
        float bf = (Float.parseFloat(edCF_67291.getText().toString()));//макс 130кг
        float ba = (Float.parseFloat(edAF_67291.getText().toString()));//макс 227кг

        if ((edR1_67291.getText().toString().equals(""))) {
            edR1_67291.setText("0");
        } else if ((edR2_67291.getText().toString().equals(""))) {
            edR2_67291.setText("0");
        } else if ((edR3_67291.getText().toString().equals(""))) {
            edR3_67291.setText("0");
        } else if ((edR4_67291.getText().toString().equals(""))) {
            edR4_67291.setText("0");
        } else if ((edR5_67291.getText().toString().equals(""))) {
            edR5_67291.setText("0");
        } else if ((edR6_67291.getText().toString().equals(""))) {
            edR6_67291.setText("0");
        } else if ((edR7_67291.getText().toString().equals(""))) {
            edR7_67291.setText("0");
        }

        if (cbLeto_67291_dhc.isChecked()) {
            if (r1 > 225) {
                Toast toast4 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                toast4.show();
            } else if (r2 > 225) {
                Toast toast5 = Toast.makeText(getActivity(), "Второй ряд перебор!", Toast.LENGTH_LONG);
                toast5.show();
            } else if (r3 > 225) {
                Toast toast6 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                toast6.show();
            } else if (r4 > 225) {
                Toast toast5 = Toast.makeText(getActivity(), "Четвертый ряд перебор!", Toast.LENGTH_LONG);
                toast5.show();
            } else if (r5 > 225) {
                Toast toast6 = Toast.makeText(getActivity(), "Пятый ряд перебор!", Toast.LENGTH_LONG);
                toast6.show();
            } else if (r6 > 75) {
                Toast toast5 = Toast.makeText(getActivity(), "Шестой ряд перебор!", Toast.LENGTH_LONG);
                toast5.show();
            } else if (r7 > 225) {
                Toast toast6 = Toast.makeText(getActivity(), "Седьмой ряд перебор!", Toast.LENGTH_LONG);
                toast6.show();
            }


        } else {
            if (r1 > 240) {
                Toast toast4 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                toast4.show();
            } else if (r2 > 240) {
                Toast toast5 = Toast.makeText(getActivity(), "Второй ряд перебор!", Toast.LENGTH_LONG);
                toast5.show();
            } else if (r3 > 240) {
                Toast toast6 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                toast6.show();
            } else if (r4 > 240) {
                Toast toast5 = Toast.makeText(getActivity(), "Четвертый ряд перебор!", Toast.LENGTH_LONG);
                toast5.show();
            } else if (r5 > 240) {
                Toast toast6 = Toast.makeText(getActivity(), "Пятый ряд перебор!", Toast.LENGTH_LONG);
                toast6.show();
            } else if (r6 > 80) {
                Toast toast5 = Toast.makeText(getActivity(), "Шестой ряд перебор!", Toast.LENGTH_LONG);
                toast5.show();
            } else if (r7 > 240) {
                Toast toast6 = Toast.makeText(getActivity(), "Седьмой ряд перебор!", Toast.LENGTH_LONG);
                toast6.show();
            }
        }

        float gVzl = gPust + (float) gEk + (float) gFuel_p + (float) gFuel_z + r1 + r2 + r3 + r4 + r5 + r6 + r7 + bf + ba - (float) gZem;
        edVes67291_kg_dhc.setText(String.valueOf((int) gVzl));
        edVes67291_ft_dhc.setText(String.valueOf(round(gVzl * 2.2046223302272)));
        edVes67291_kg_dhc.setTextColor(BLACK);
        edVes67291_ft_dhc.setTextColor(BLACK);
        int tt = (Integer.parseInt(edtopl67291_p_dhc.getText().toString())) + (Integer.parseInt(edtopl67291_z_dhc.getText().toString())) ;
        edtopl67291_ob_dhc.setText(String.valueOf(tt));
        float gPred = 5715 - (gPust + (float) gEk + (float) gFuel_p + (float) gFuel_z + 45);
        edPred67291_dhc.setTextColor(BLUE);
        edPred67291_dhc.setText(String.valueOf((int) gPred));
        if (gVzl > 5670) {
            edVes67291_kg_dhc.setTextColor(RED);
            edVes67291_ft_dhc.setTextColor(RED);
            Toast toast10 = Toast.makeText(getActivity(), "Превышение\nвзлётного\nвеса!!!", Toast.LENGTH_LONG);
            toast10.show();
        }
        /////Далее центровка
        float cG = (1657875f + + (((float) gEk * 2.2046223302272f) * 95.0f) + (((float) (gFuel_p - 22.5) * 2.2046223302272f) * 163.0f) + (((float) (gFuel_z - 22.5) * 2.2046223302272f) * 239.0f)
                + ((r1 * 2.2046223302272f) * 135.0f) + ((r2 * 2.2046223302272f) * 165.0f) + ((r3 * 2.2046223302272f) * 195.0f)
                + ((r4 * 2.2046223302272f) * 225.0f) + ((r5 * 2.2046223302272f) * 254.0f)
                + ((r6 * 2.2046223302272f) * 281.0f) + ((r7 * 2.2046223302272f) * 320.0f)
                + ((bf * 2.2046223302272f) * 32.00f) + ((ba * 2.2046223302272f) * 354.0f))
                / (gVzl * 2.2046223302272f);
        float cEntr = (cG - 188.24f) / 0.78f;
        String formattedDouble = new DecimalFormat("#0.00").format(cG);
        edCG67291.setText(formattedDouble);//вывод плеча CG location(для графика)
        edCentr67291_dhc.setTextColor(BLACK);
        edCentr67291_dhc.setText(String.valueOf((Math.floor(cEntr * 100f + 0.5f) / 100f)));
        //Предельные центровки общие
        if (cEntr > 36.00) {
            edCentr67291_dhc.setTextColor(RED);
            Toast toast10 = Toast.makeText(getActivity(), "Предельно\nзадняя\nцентровка!!", Toast.LENGTH_LONG);
            toast10.show();
        }
        if ((cEntr < 20) & (gVzl < 5262)) {
            float сMin = ((gVzl - 2327.6885245902f) / 54.6769527483f);
            if (cEntr < сMin) {
                edCentr67291_dhc.setTextColor(RED);
                Toast toast10 = Toast.makeText(getActivity(), "Предельно\nпередняя\nцентровка!!", Toast.LENGTH_LONG);
                toast10.show();
            }
        }
        //Предельные центровки для посадки по весу

        if (cbPos_67291_dhc.isChecked()) {
            float CentrMin = ((gVzl - 2630f) / 118f);
            if (cEntr < CentrMin) {
                edCentr67291_dhc.setTextColor(RED);
                Toast toast11 = Toast.makeText(getActivity(), "Предельно\nпередняя\nцентровка для посадки!!", Toast.LENGTH_LONG);
                toast11.show();
            }
            if (gVzl > 5579) {
                edVes67291_kg_dhc.setTextColor(RED);
                edVes67291_ft_dhc.setTextColor(RED);
                Toast toast12 = Toast.makeText(getActivity(), "Превышение\nпосадочного\nвеса!!", Toast.LENGTH_LONG);
                toast12.show();
            }
            //Предельные центровки для взлёта по весу

        } else if (gVzl > 5262) {
            float CentrMin = ((gVzl - 3630f) / 81.6f);
            if (cEntr < CentrMin) {
                edCentr67291_dhc.setTextColor(RED);
                Toast toast13 = Toast.makeText(getActivity(), "Предельно\nпередняя\nцентровка для взлёта!!", Toast.LENGTH_LONG);
                toast13.show();
            }
        }
    }
}
//алес :):)

