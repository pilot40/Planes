package pilot40.planes.pakage.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.DecimalFormat;

import static java.lang.Math.round;
import static java.lang.Math.sin;


import pilot40.planes.pakage.R;


public class FragmentVeter208 extends Fragment {
    public Spinner spinScep;
    public Spinner spinVet;
    public EditText edUv_208, edU_208, edUbok_208, edMs_208, edKph_208;
    public Button btBokRas_208, btMsRas_208;
    //ArrayAdapter<CharSequence> adapspinScep;

    public FragmentVeter208() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_veter_208, container, false);

    }

    @Override
    public void onStart() {
        super.onStart();

        btBokRas_208 = (Button)  getActivity().findViewById(R.id.btBokRas_208);
        btMsRas_208 = (Button)  getActivity().findViewById(R.id.btMsRas_208);

        edUv_208 = (EditText)  getActivity().findViewById(R.id.edUv_208);
        edU_208 = (EditText)  getActivity().findViewById(R.id.edU_208);
        edUbok_208 = (EditText)  getActivity().findViewById(R.id.edUbok_208);
        edMs_208 = (EditText)  getActivity().findViewById(R.id.edMs_208);
        edKph_208 = (EditText)  getActivity().findViewById(R.id.edKph_208);

        spinScep = (Spinner) getActivity().findViewById(R.id.spinScep);
        spinVet = (Spinner) getActivity().findViewById(R.id.spinVet);

        ArrayAdapter<CharSequence> adapspinScep = ArrayAdapter.createFromResource(getActivity(),
                R.array.scep, android.R.layout.simple_spinner_item);
        adapspinScep.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinScep.setAdapter(adapspinScep);

        ArrayAdapter<CharSequence> adapspinVet = ArrayAdapter.createFromResource(getActivity(),
                R.array.veter, android.R.layout.simple_spinner_item);
        adapspinVet.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinVet.setAdapter(adapspinVet);



        spinScep.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                int sCep = spinScep.getSelectedItemPosition();
                spinVet.setSelection(sCep);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinVet.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                int vEt = spinVet.getSelectedItemPosition();
                spinScep.setSelection(vEt);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btBokRas_208.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edUbok_208.setText("0.0");
                float u = (Float.parseFloat(edU_208.getText().toString()));
                float uv = (Float.parseFloat(edUv_208.getText().toString()));
                float ub = (float)(sin(uv* 3.14159265 / 180))*u;
                String formattedDouble = new DecimalFormat("#0.0").format(ub);
                edUbok_208.setText(formattedDouble);
                //ui->lineUB->setText(QString::number((floor(ub*10+.5)/10)));
            }
        });

        btMsRas_208.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edKph_208.setText("0");
                float vmc = (Float.parseFloat(edMs_208.getText().toString()));
                float vkm = ((vmc*3600)/1000);
                edKph_208.setText(String.valueOf(round(vkm)));//вывод км/ч
            }
        });


    }

}
