package pilot40.planes.pakage.fragments;

import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import pilot40.planes.pakage.R;


import static android.graphics.Color.BLACK;
import static android.graphics.Color.BLUE;
import static android.graphics.Color.RED;


public class Fragment67422_sz1_208 extends Fragment implements View.OnClickListener{

    public   EditText edGob, edTopl67422_sz2;
    private TextView r1_67422_sz1, r2_67422_sz1, r3_67422_sz1, n1_67422_sz1, bagA67422_sz1, bagB67422_sz1, bagC67422_sz1, bagD67422_sz1, b4_67422_sz1, b6_67422_sz1;
    private EditText edTopl67422_sz1, edA67422_sz1, edB67422_sz1, edC67422_sz1, edD67422_sz1, edPred67422_sz1, edVes67422_sz1, edTks67422_sz1, edEk67422_sz1,
            edR1_67422_sz1, edR2_67422_sz1, edR3_67422_sz1, edN167422_sz1, edB4_67422_sz1, edB6_67422_sz1, edCentr67422_sz1, edCG67422_sz1;
    private CheckBox cbLeto_67422_sz1;
    private FloatingActionButton fb_67422_sz1;

    public Fragment67422_sz1_208() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_67422_sz1, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();


        Button btRassch67422_sz1 = (Button) getActivity().findViewById(R.id.btRassch67422_sz1);

        cbLeto_67422_sz1 = (CheckBox) getActivity().findViewById(R.id.cbLeto_67422_sz1);

        edTopl67422_sz1 = (EditText) getActivity().findViewById(R.id.edTopl67422_sz1);
        edA67422_sz1 = (EditText) getActivity().findViewById(R.id.edA67422_sz1);
        edB67422_sz1 = (EditText) getActivity().findViewById(R.id.edB67422_sz1);
        edC67422_sz1 = (EditText) getActivity().findViewById(R.id.edC67422_sz1);
        edD67422_sz1 = (EditText) getActivity().findViewById(R.id.edD67422_sz1);
        edTks67422_sz1 = (EditText) getActivity().findViewById(R.id.edTks67422_sz1);
        edPred67422_sz1 = (EditText) getActivity().findViewById(R.id.edPred67422_sz1);
        edVes67422_sz1 = (EditText) getActivity().findViewById(R.id.edVes67422_sz1);
        edEk67422_sz1 = (EditText) getActivity().findViewById(R.id.edEk67422_sz1);
        edR1_67422_sz1 = (EditText) getActivity().findViewById(R.id.edR1_67422_sz1);
        edR2_67422_sz1 = (EditText) getActivity().findViewById(R.id.edR2_67422_sz1);
        edR3_67422_sz1 = (EditText) getActivity().findViewById(R.id.edR3_67422_sz1);
        edN167422_sz1 = (EditText) getActivity().findViewById(R.id.edN167422_sz1);
        edB4_67422_sz1 = (EditText) getActivity().findViewById(R.id.edB4_67422_sz1);
        edB6_67422_sz1 = (EditText) getActivity().findViewById(R.id.edB6_67422_sz1);
        edCentr67422_sz1 = (EditText) getActivity().findViewById(R.id.edCentr67422_sz1);
        edCG67422_sz1 = (EditText) getActivity().findViewById(R.id.edCG67422_sz1);
        r1_67422_sz1 = (TextView) getActivity().findViewById(R.id.r1_67422_sz1);
        r2_67422_sz1 = (TextView) getActivity().findViewById(R.id.r2_67422_sz1);
        r3_67422_sz1 = (TextView) getActivity().findViewById(R.id.r3_67422_sz1);
        n1_67422_sz1 = (TextView) getActivity().findViewById(R.id.n1_67422_sz1);
        bagA67422_sz1 = (TextView) getActivity().findViewById(R.id.bagA67422_sz1);
        bagB67422_sz1 = (TextView) getActivity().findViewById(R.id.bagB67422_sz1);
        bagC67422_sz1 = (TextView) getActivity().findViewById(R.id.bagC67422_sz1);
        bagD67422_sz1 = (TextView) getActivity().findViewById(R.id.bagD67422_sz1);
        b4_67422_sz1 = (TextView) getActivity().findViewById(R.id.b4_67422_sz1);
        b6_67422_sz1 = (TextView) getActivity().findViewById(R.id.b6_67422_sz1);


        fb_67422_sz1 = (FloatingActionButton) getActivity().findViewById(R.id.fb_67422_sz1);
        fb_67422_sz1.show();

        fb_67422_sz1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edR1_67422_sz1.setText("0");
                edR2_67422_sz1.setText("0");
                edR3_67422_sz1.setText("0");
                edN167422_sz1.setText("0");
                edA67422_sz1.setText("0");
                edB67422_sz1.setText("0");
                edC67422_sz1.setText("0");
                edD67422_sz1.setText("0");
                edB4_67422_sz1.setText("0");
                edB6_67422_sz1.setText("0");
                edCentr67422_sz1.setText("0");
                edCG67422_sz1.setText("0");
                edVes67422_sz1.setText("0");
                edPred67422_sz1.setText("0");
            }
        });

        try {
            edGob = (EditText) getActivity().findViewById(R.id.edGob);
            edTopl67422_sz1.setText(String.valueOf(edGob.getText().toString()));
        } catch (NullPointerException e) {

        }
        try {
            edTopl67422_sz2 = (EditText) getActivity().findViewById(R.id.edTopl67422_sz2);
            edTopl67422_sz1.setText(String.valueOf(edTopl67422_sz2.getText().toString()));
        } catch (NullPointerException e) {
        }


        if ((edTopl67422_sz1.getText().toString().equals(""))) {
            Toast toast2 = Toast.makeText(getActivity(), "Вес топлива не введён", Toast.LENGTH_LONG);
            toast2.show();
            edTopl67422_sz1.setText("0");
        } else if (edR1_67422_sz1.getText().toString().equals("")) {
            edR1_67422_sz1.setText("0");
        } else if (edR2_67422_sz1.getText().toString().equals("")) {
            edR2_67422_sz1.setText("0");
        } else if (edR3_67422_sz1.getText().toString().equals("")) {
            edR3_67422_sz1.setText("0");
        } else if (edN167422_sz1.getText().toString().equals("")) {
            edN167422_sz1.setText("0");
        }

        r1_67422_sz1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR1_67422_sz1.getText().toString());
                if (cbLeto_67422_sz1.isChecked()) {
                    if (c < 225) {
                        edR1_67422_sz1.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }

                } else {
                    if (c < 240) {
                        edR1_67422_sz1.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        r2_67422_sz1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR2_67422_sz1.getText().toString());
                if (cbLeto_67422_sz1.isChecked()) {
                    if (c < 150) {
                        edR2_67422_sz1.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Второй ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                } else {
                    if (c < 160) {
                        edR2_67422_sz1.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Второй ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        r3_67422_sz1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR3_67422_sz1.getText().toString());
                if (cbLeto_67422_sz1.isChecked()) {
                    if (c < 75) {
                        edR3_67422_sz1.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                } else {
                    if (c < 80) {
                        edR3_67422_sz1.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        n1_67422_sz1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edN167422_sz1.getText().toString());
                if (cbLeto_67422_sz1.isChecked()) {
                    if (c < 75) {
                        edN167422_sz1.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Носилки перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                } else {
                    if (c < 80) {
                        edN167422_sz1.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Носилки перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        bagA67422_sz1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edA67422_sz1.getText().toString());
                if (c <= 104) {
                    edA67422_sz1.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Багаж А перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }

            }
        });

        bagB67422_sz1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edB67422_sz1.getText().toString());
                if (c <= 141) {
                    edB67422_sz1.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Багаж B перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        bagC67422_sz1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edC67422_sz1.getText().toString());
                if (c <= 123) {
                    edC67422_sz1.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Багаж C перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        bagD67422_sz1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edD67422_sz1.getText().toString());
                if (c <= 127) {
                    edD67422_sz1.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Багаж D перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        b4_67422_sz1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edB4_67422_sz1.getText().toString());
                if (c <= 154) {
                    edB4_67422_sz1.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Зона B4 перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        b6_67422_sz1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edB6_67422_sz1.getText().toString());
                if (c <= 154) {
                    edB6_67422_sz1.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Зона B6 перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        btRassch67422_sz1.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if ((edTopl67422_sz1.getText().toString().equals(""))) {
            Toast toast2 = Toast.makeText(getActivity(), "Вес топлива не введён", Toast.LENGTH_LONG);
            toast2.show();
            edTopl67422_sz1.setText("0");
        }
        if ((edTks67422_sz1.getText().toString().equals(""))) {
            edTks67422_sz1.setText("0");
        }
        if ((edA67422_sz1.getText().toString().equals(""))) {
            edA67422_sz1.setText("0");
        }
        if ((edB67422_sz1.getText().toString().equals(""))) {
            edB67422_sz1.setText("0");
        }
        if ((edC67422_sz1.getText().toString().equals(""))) {
            edC67422_sz1.setText("0");
        }
        if ((edD67422_sz1.getText().toString().equals(""))) {
            edD67422_sz1.setText("0");
        }
        if ((edB4_67422_sz1.getText().toString().equals(""))) {
            edB4_67422_sz1.setText("0");
        }
        if ((edB6_67422_sz1.getText().toString().equals(""))) {
            edB6_67422_sz1.setText("0");
        }
        if ((edR1_67422_sz1.getText().toString().equals(""))) {
            edR1_67422_sz1.setText("0");
        }
        if ((edR2_67422_sz1.getText().toString().equals(""))) {
            edR2_67422_sz1.setText("0");
        }
        if ((edR3_67422_sz1.getText().toString().equals(""))) {
            edR3_67422_sz1.setText("0");
        }
        if ((edN167422_sz1.getText().toString().equals(""))) {
            edN167422_sz1.setText("0");
        }

        float gPust = 2368;//вес пустого кг
        int gSl = 38;
        int gZem = 16;
        int gFuel = Integer.parseInt(edTopl67422_sz1.getText().toString());
        int vEk = (Integer.parseInt(edEk67422_sz1.getText().toString())) * 80;
        float tks1 = (Float.parseFloat(edTks67422_sz1.getText().toString())) * 4.1731f;
        float tks = (Float.parseFloat(edTks67422_sz1.getText().toString()));
        float r1 = (Float.parseFloat(edR1_67422_sz1.getText().toString()));
        float r2 = (Float.parseFloat(edR2_67422_sz1.getText().toString()));
        float r3 = (Float.parseFloat(edR3_67422_sz1.getText().toString()));
        float n1 = (Float.parseFloat(edN167422_sz1.getText().toString()));
        float a = (Float.parseFloat(edA67422_sz1.getText().toString()));
        float b = (Float.parseFloat(edB67422_sz1.getText().toString()));
        float c = (Float.parseFloat(edC67422_sz1.getText().toString()));
        float d = (Float.parseFloat(edD67422_sz1.getText().toString()));
        float b4 = (Float.parseFloat(edB4_67422_sz1.getText().toString()));
        float b6 = (Float.parseFloat(edB6_67422_sz1.getText().toString()));

        if ((edR1_67422_sz1.getText().toString().equals(""))) {
            edR1_67422_sz1.setText("0");
        } else if ((edR2_67422_sz1.getText().toString().equals(""))) {
            edR2_67422_sz1.setText("0");
        } else if ((edR3_67422_sz1.getText().toString().equals(""))) {
            edR3_67422_sz1.setText("0");
        } else if ((edN167422_sz1.getText().toString().equals(""))) {
            edN167422_sz1.setText("0");
        }

        if (cbLeto_67422_sz1.isChecked()) {
            if (r1 > 225) {
                Toast toast4 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                toast4.show();
            } else if (r2 > 150) {
                Toast toast5 = Toast.makeText(getActivity(), "Второй ряд перебор!", Toast.LENGTH_LONG);
                toast5.show();
            } else if (r3 > 75) {
                Toast toast6 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                toast6.show();
            } else if (n1 > 75) {
                Toast toast6 = Toast.makeText(getActivity(), "Носилки перебор!", Toast.LENGTH_LONG);
                toast6.show();
            }
        } else {
            if (r1 > 240) {
                Toast toast7 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                toast7.show();
            } else if (r2 > 160) {
                Toast toast8 = Toast.makeText(getActivity(), "Второй ряд перебор!", Toast.LENGTH_LONG);
                toast8.show();
            } else if (r3 > 80) {
                Toast toast9 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                toast9.show();
            } else if (n1 > 80) {
                Toast toast9 = Toast.makeText(getActivity(), "Носилки перебор!", Toast.LENGTH_LONG);
                toast9.show();
            }
        }

        float gVzl = gPust + (float) gSl + (float) gFuel + tks1 + (float) vEk + r1 + r2 + r3 + n1 + a + b + c + d + b4 + b6 - (float) gZem;
        edVes67422_sz1.setText(String.valueOf((int) gVzl));
        edVes67422_sz1.setTextColor(BLACK);
        float gPred = 3969 - (gPust + (float) gSl + (float) gFuel + tks1 + (float) vEk - (float) gZem);
        edPred67422_sz1.setTextColor(BLUE);
        edPred67422_sz1.setText(String.valueOf((int) gPred));
        if (gVzl > 3969) {
            edVes67422_sz1.setTextColor(RED);
            Toast toast10 = Toast.makeText(getActivity(), "Превышение\nвзлётного\nвеса!!!", Toast.LENGTH_LONG);
            toast10.show();
        }
        /////Далее центровка
        float cG = (((1004.53f + 22.35f + (((float) gFuel / 0.4536f) / 4.92f)
                + 23.90f + 23.90f + (((r1 / 0.4536f) * 192.6f) / 1000f)
                + (((r2 / 0.4536f) * 229.21f) / 1000f) + (((r3 / 0.4536f) * 295.0f) / 1000f) + (((n1 / 0.4536f) * 245.3f) / 1000f)
                + (((a / 0.4536f) * 132.4f) / 1000f) + (((b / 0.4536f) * 182.1f) / 1000f)
                + (((c / 0.4536f) * 233.4f) / 1000f) + (((d / 0.4536f) * 287.6f) / 1000f)
                + (((b4 / 0.4536f) * 294.5f) / 1000f) + (((b6 / 0.4536f) * 344f) / 1000f)
                + (((tks * 9.2f) * 195.77f) / 1000f) - 7.11f) / (gVzl / 0.4536f)) * 1000f);
        float cEntr = (cG - 177.57f) / 0.664f;
        String formattedDouble = new DecimalFormat("#0.00").format(cG);
        edCG67422_sz1.setText(formattedDouble);//вывод плеча CG location(для графика)
        //edCG67422_sz1.setText(String.valueOf(Math.floor(cG)));//вывод плеча CG location(для графика)
        edCentr67422_sz1.setTextColor(BLACK);
        edCentr67422_sz1.setText(String.valueOf((Math.floor(cEntr * 100f + 0.5f) / 100f)));

        if (cEntr > 40.33) {
            edCentr67422_sz1.setTextColor(RED);
            Toast toast10 = Toast.makeText(getActivity(), "Предельно\nзадняя\nцентровка!!", Toast.LENGTH_LONG);
            toast10.show();
        }
        if (gVzl <= 3629) {
            float сMin = ((gVzl - 2327.6885245902f) / 54.6769527483f);
            if (cEntr < сMin) {
                edCentr67422_sz1.setTextColor(RED);
                Toast toast10 = Toast.makeText(getActivity(), "Предельно\nпередняя\nцентровка!!", Toast.LENGTH_LONG);
                toast10.show();
            }
        } else if ((gVzl > 3629) & (gVzl <= 3969)) {
            float cMin = ((gVzl - 2698.8850574713f) / 39.0804597701f);
            if (cEntr < cMin) {
                edCentr67422_sz1.setTextColor(RED);
                Toast toast10 = Toast.makeText(getActivity(), "Предельно\nпередняя\nцентровка!!", Toast.LENGTH_LONG);
                toast10.show();
            }
        }
    }

}
