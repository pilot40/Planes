package pilot40.planes.pakage.fragments;


import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import pilot40.planes.pakage.R;


public class FragmentMetar extends Fragment {

    public FragmentMetar() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_metar, container, false);
        WebView webview = (WebView) v.findViewById(R.id.metar);
        webview.setVerticalScrollBarEnabled(true);
        webview.setBackgroundColor(Color.parseColor("#ffffff"));
        webview.getSettings().setBuiltInZoomControls(true);
        webview.setWebViewClient(new MyWebViewClient());
        webview.loadUrl("http://pilot208.ru/metar/un.php");


        return v;
    }

    private class MyWebViewClient extends WebViewClient {

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView webview, String url)
        {
            webview.loadUrl(url);
            return true;
        }
    }
}
