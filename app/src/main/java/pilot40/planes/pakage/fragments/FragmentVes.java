package pilot40.planes.pakage.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


import pilot40.planes.pakage.R;

import static java.lang.Math.round;



public class FragmentVes extends Fragment {

    public EditText fntkg_f, fntkg_k, kgfnt_k, kgfnt_f;
    public Button btn_fntkg, btn_kgfnt;


    public FragmentVes() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_ves, container, false);

    }

    @Override
    public void onStart() {
        super.onStart();

        btn_fntkg = (Button)  getActivity().findViewById(R.id.btn_fntkg);
        btn_kgfnt = (Button)  getActivity().findViewById(R.id.btn_kgfnt);

        fntkg_f = (EditText)  getActivity().findViewById(R.id.fntkg_f);
        fntkg_k = (EditText)  getActivity().findViewById(R.id.fntkg_k);
        kgfnt_k = (EditText)  getActivity().findViewById(R.id.kgfnt_k);
        kgfnt_f = (EditText)  getActivity().findViewById(R.id.kgfnt_f);


        btn_fntkg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fntkg_k.setText("0");
                float  ft = (Float.parseFloat(fntkg_f.getText().toString()));
                float kg = round(ft*0.45359237f);
                fntkg_k.setText(String.valueOf((int)kg));
            }
        });

        btn_kgfnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                kgfnt_f.setText("0");
                float  kg = (Float.parseFloat(kgfnt_k.getText().toString()));
                float ft = round(kg*2.2046223302272f);
                kgfnt_f.setText(String.valueOf((int)ft));
            }
        });


    }

}
