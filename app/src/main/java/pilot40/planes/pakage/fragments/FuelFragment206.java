package pilot40.planes.pakage.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pilot40.planes.pakage.R;

public class FuelFragment206 extends Fragment implements View.OnClickListener {


    public  EditText edRasst206, edZapas206, edSpeed206, edGpol206, edGzap206, edTime206, edGob206;




    public FuelFragment206() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_fuel_206, container, false);
    }



    @Override
    public void onStart() {
        super.onStart();
        Button btn = (Button) getActivity().findViewById(R.id.button_rasch_206);

        edRasst206 = (EditText) getActivity().findViewById(R.id.edRasst206);
        edZapas206 = (EditText) getActivity().findViewById(R.id.edZapas206);
        edSpeed206 = (EditText) getActivity().findViewById(R.id.edSpeed206);
        edGob206 = (EditText) getActivity().findViewById(R.id.edGob206);
        edGpol206 = (EditText) getActivity().findViewById(R.id.edGpol206);
        edGzap206 = (EditText) getActivity().findViewById(R.id.edGzap206);
        edTime206 = (EditText) getActivity().findViewById(R.id.edTime206);

        btn.setOnClickListener(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        setRetainInstance(true);
    }

    @Override
    public void onClick(View view) {

        if (((edRasst206.getText().toString().equals(""))) | (edZapas206.getText().toString().equals(""))) {
            Toast toast = Toast.makeText(getActivity(), "Введите значения!", Toast.LENGTH_LONG);
            toast.show();
        } else {
            int V = Integer.parseInt(edSpeed206.getText().toString());
            int S = Integer.parseInt(edRasst206.getText().toString());
            int Salt = Integer.parseInt(edZapas206.getText().toString());


            float Tzap = (float) Salt / V;//Время полета до запасного
            float Tp = (float) S / V; //Время полета до ап
            int hours = (int) Tp;
            int minutes = (int) (60 * (Tp - hours));
            double Gpol = (Tp * 50 * 0.03) + (Tp * 50);
            double Gzap = (Tzap * 50) + 38;
            double Gf = Gpol + Gzap;
            edGpol206.setText(String.valueOf((int) Gpol));
            edGzap206.setText(String.valueOf((int) Gzap));
            edGob206.setText(String.valueOf((int) Gf));
            ///надо как то передать это в расчёт веса. ПЕРЕДАЛ!!!!!!

            if ((hours < 10) & (minutes < 10)) {
                edTime206.setText("0" + hours + ":0" + minutes);
            } else if ((hours < 10) & (minutes >= 10)) {
                edTime206.setText("0" + hours + ":" + minutes);
            }



        }


    }


}


