package pilot40.planes.pakage.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import pilot40.planes.pakage.R;

import static java.lang.Math.round;


public class FragmentPress extends Fragment {

    public EditText mm_press, gpa_press, gpa_alt, h_alt,gpa_aer;
    public Button btn_press, btn_alt;


    public FragmentPress() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_press, container, false);

    }

    @Override
    public void onStart() {
        super.onStart();

        btn_press = (Button)  getActivity().findViewById(R.id.btn_press);
        btn_alt = (Button)  getActivity().findViewById(R.id.btn_alt);

        mm_press = (EditText)  getActivity().findViewById(R.id.mm_press);
        gpa_press = (EditText)  getActivity().findViewById(R.id.gpa_press);
        gpa_alt = (EditText)  getActivity().findViewById(R.id.gpa_alt);
        h_alt = (EditText)  getActivity().findViewById(R.id.h_alt);
        gpa_aer = (EditText)  getActivity().findViewById(R.id.gpa_aer);



        btn_press.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gpa_press.setText("0");
                float  mm = (Float.parseFloat(mm_press.getText().toString()));
                float gp = round(mm*1.33322f);
                gpa_press.setText(String.valueOf((int)gp));
                gpa_alt.setText(String.valueOf(gpa_press.getText().toString()));
            }
        });

        btn_alt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                gpa_aer.setText("0");
                float  pa_aer = (Float.parseFloat(gpa_alt.getText().toString()));
                float h_aer = (Float.parseFloat(h_alt.getText().toString()));
                float gp_aer = round(pa_aer-(h_aer/8f));
                gpa_aer.setText(String.valueOf((int)gp_aer));
            }
        });


    }

}
