package pilot40.planes.pakage.fragments;

import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import pilot40.planes.pakage.R;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.BLUE;
import static android.graphics.Color.RED;


public class Fragment67450_rab_208 extends Fragment implements View.OnClickListener{

    public EditText edGob, edTopl67450_sz1,edTopl67422_sz2;
    private TextView r1_67450rab, r3_67450rab, bagA67450rab, bagB67450rab, bagC67450rab, bagD67450rab, b4_67450rab, b6_67450rab;
    private EditText edTopl67450rab, edA67450rab, edB67450rab, edC67450rab, edD67450rab, edPred67450rab, edVes67450rab, edTks67450rab,
            edEk67450rab, edR1_67450rab, edR3_67450rab, edB4_67450rab, edB6_67450rab, edCentr67450rab, edCG67450rab;
    private CheckBox cbLeto_67450_rab;
    private FloatingActionButton fb_67450_rab;

    public Fragment67450_rab_208() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setRetainInstance(true);        
        return inflater.inflate(R.layout.fragment_67450_rab, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();


        Button btRassch67450rab = (Button) getActivity().findViewById(R.id.btRassch67450rab);

        cbLeto_67450_rab = (CheckBox) getActivity().findViewById(R.id.cbLeto_67450_rab);

        edTopl67450rab = (EditText) getActivity().findViewById(R.id.edTopl67450rab);
        edA67450rab = (EditText) getActivity().findViewById(R.id.edA67450rab);
        edB67450rab = (EditText) getActivity().findViewById(R.id.edB67450rab);
        edC67450rab = (EditText) getActivity().findViewById(R.id.edC67450rab);
        edD67450rab = (EditText) getActivity().findViewById(R.id.edD67450rab);
        edTks67450rab = (EditText) getActivity().findViewById(R.id.edTks67450rab);
        edPred67450rab = (EditText) getActivity().findViewById(R.id.edPred67450rab);
        edVes67450rab = (EditText) getActivity().findViewById(R.id.edVes67450rab);
        edEk67450rab = (EditText) getActivity().findViewById(R.id.edEk67450rab);
        edR1_67450rab = (EditText) getActivity().findViewById(R.id.edR1_67450rab);

        edR3_67450rab = (EditText) getActivity().findViewById(R.id.edR3_67450rab);
        edB4_67450rab = (EditText) getActivity().findViewById(R.id.edB4_67450rab);
        edB6_67450rab = (EditText) getActivity().findViewById(R.id.edB6_67450rab);
        edCentr67450rab = (EditText) getActivity().findViewById(R.id.edCentr67450rab);
        edCG67450rab = (EditText) getActivity().findViewById(R.id.edCG67450rab);
        r1_67450rab = (TextView) getActivity().findViewById(R.id.r1_67450rab);

        r3_67450rab = (TextView) getActivity().findViewById(R.id.r3_67450rab);
        bagA67450rab = (TextView) getActivity().findViewById(R.id.bagA67450rab);
        bagB67450rab = (TextView) getActivity().findViewById(R.id.bagB67450rab);
        bagC67450rab = (TextView) getActivity().findViewById(R.id.bagC67450rab);
        bagD67450rab = (TextView) getActivity().findViewById(R.id.bagD67450rab);
        b4_67450rab = (TextView) getActivity().findViewById(R.id.b4_67450rab);
        b6_67450rab = (TextView) getActivity().findViewById(R.id.b6_67450rab);


        fb_67450_rab = (FloatingActionButton) getActivity().findViewById(R.id.fb_67450_rab);
        fb_67450_rab.show();

        fb_67450_rab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edR1_67450rab.setText("0");

                edR3_67450rab.setText("0");
                edA67450rab.setText("0");
                edB67450rab.setText("0");
                edC67450rab.setText("0");
                edD67450rab.setText("0");
                edB4_67450rab.setText("0");
                edB6_67450rab.setText("0");
                edCentr67450rab.setText("0");
                edCG67450rab.setText("0");
                edPred67450rab.setText("0");
                edVes67450rab.setText("0");
            }
        });

        try {
            edGob = (EditText) getActivity().findViewById(R.id.edGob);
            edTopl67450rab.setText(String.valueOf(edGob.getText().toString()));
        } catch (NullPointerException e) {
        }
        try {
            edTopl67450_sz1 = (EditText) getActivity().findViewById(R.id.edTopl67450_sz1);
            edTopl67450rab.setText(String.valueOf(edTopl67450_sz1.getText().toString()));
        } catch (NullPointerException e) {
        }
        try {
            edTopl67422_sz2 = (EditText) getActivity().findViewById(R.id.edTopl67422_sz2);
            edTopl67450rab.setText(String.valueOf(edTopl67422_sz2.getText().toString()));
        } catch (NullPointerException e) {
        }

        if ((edTopl67450rab.getText().toString().equals(""))) {
            Toast toast2 = Toast.makeText(getActivity(), "Вес топлива не введён", Toast.LENGTH_LONG);
            toast2.show();
            edTopl67450rab.setText("0");
        } else if (edR1_67450rab.getText().toString().equals("")) {
            edR1_67450rab.setText("0");

        } else if (edR3_67450rab.getText().toString().equals("")) {
            edR3_67450rab.setText("0");
        }

        r1_67450rab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR1_67450rab.getText().toString());
                if (cbLeto_67450_rab.isChecked()) {
                    if (c < 225) {
                        edR1_67450rab.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }

                } else {
                    if (c < 240) {
                        edR1_67450rab.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });


        r3_67450rab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int c = Integer.parseInt(edR3_67450rab.getText().toString());
                if (cbLeto_67450_rab.isChecked()) {
                    if (c < 150) {
                        edR3_67450rab.setText(String.valueOf(c + 75));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                } else {
                    if (c < 160) {
                        edR3_67450rab.setText(String.valueOf(c + 80));
                    } else {
                        Toast toast4 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                        toast4.show();
                    }
                }
            }
        });

        bagA67450rab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edA67450rab.getText().toString());
                if (c <= 104) {
                    edA67450rab.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Багаж А перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }

            }
        });

        bagB67450rab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edB67450rab.getText().toString());
                if (c <= 141) {
                    edB67450rab.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Багаж B перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        bagC67450rab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edC67450rab.getText().toString());
                if (c <= 123) {
                    edC67450rab.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Багаж C перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        bagD67450rab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edD67450rab.getText().toString());
                if (c <= 127) {
                    edD67450rab.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Багаж D перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        b4_67450rab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edB4_67450rab.getText().toString());
                if (c <= 154) {
                    edB4_67450rab.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Зона B4 перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        b6_67450rab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int c = Integer.parseInt(edB6_67450rab.getText().toString());
                if (c <= 154) {
                    edB6_67450rab.setText(String.valueOf(c + 10));
                } else {
                    Toast toast4 = Toast.makeText(getActivity(), "Зона B6 перебор!", Toast.LENGTH_LONG);
                    toast4.show();
                }
            }
        });

        btRassch67450rab.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if ((edTopl67450rab.getText().toString().equals(""))) {
            Toast toast2 = Toast.makeText(getActivity(), "Вес топлива не введён", Toast.LENGTH_LONG);
            toast2.show();
            edTopl67450rab.setText("0");
        }
        if ((edTks67450rab.getText().toString().equals(""))) {
            edTks67450rab.setText("0");
        }
        if ((edA67450rab.getText().toString().equals(""))) {
            edA67450rab.setText("0");
        }
        if ((edB67450rab.getText().toString().equals(""))) {
            edB67450rab.setText("0");
        }
        if ((edC67450rab.getText().toString().equals(""))) {
            edC67450rab.setText("0");
        }
        if ((edD67450rab.getText().toString().equals(""))) {
            edD67450rab.setText("0");
        }
        if ((edB4_67450rab.getText().toString().equals(""))) {
            edB4_67450rab.setText("0");
        }
        if ((edB6_67450rab.getText().toString().equals(""))) {
            edB6_67450rab.setText("0");
        }
        if ((edR1_67450rab.getText().toString().equals(""))) {
            edR1_67450rab.setText("0");
        }

        if ((edR3_67450rab.getText().toString().equals(""))) {
            edR3_67450rab.setText("0");
        }

        float gPust = 2433;//вес пустого кг
        int gSl = 39;
        int gZem = 16;
        int gFuel = Integer.parseInt(edTopl67450rab.getText().toString());
        int vEk = (Integer.parseInt(edEk67450rab.getText().toString())) * 80;
        float tks1 = (Float.parseFloat(edTks67450rab.getText().toString())) * 4.1731f;
        float tks = (Float.parseFloat(edTks67450rab.getText().toString()));
        float r1 = (Float.parseFloat(edR1_67450rab.getText().toString()));
        float r3 = (Float.parseFloat(edR3_67450rab.getText().toString()));
        float a = (Float.parseFloat(edA67450rab.getText().toString()));
        float b = (Float.parseFloat(edB67450rab.getText().toString()));
        float c = (Float.parseFloat(edC67450rab.getText().toString()));
        float d = (Float.parseFloat(edD67450rab.getText().toString()));
        float b4 = (Float.parseFloat(edB4_67450rab.getText().toString()));
        float b6 = (Float.parseFloat(edB6_67450rab.getText().toString()));

        if ((edR1_67450rab.getText().toString().equals(""))) {
            edR1_67450rab.setText("0");
        } else if ((edR3_67450rab.getText().toString().equals(""))) {
            edR3_67450rab.setText("0");
        }

        if (cbLeto_67450_rab.isChecked()) {
            if (r1 > 225) {
                Toast toast4 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                toast4.show();
            } else if (r3 > 150) {
                Toast toast6 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                toast6.show();
            }
        } else {
            if (r1 > 240) {
                Toast toast7 = Toast.makeText(getActivity(), "Первый ряд перебор!", Toast.LENGTH_LONG);
                toast7.show();
            } else if (r3 > 160) {
                Toast toast9 = Toast.makeText(getActivity(), "Третий ряд перебор!", Toast.LENGTH_LONG);
                toast9.show();
            }
        }

        float gVzl = gPust + (float) gSl + (float) gFuel + tks1 + (float) vEk + r1 + r3 + a + b + c + d + b4 + b6 - (float) gZem;
        edVes67450rab.setText(String.valueOf((int) gVzl));
        edVes67450rab.setTextColor(BLACK);
        float gPred = 3969 - (gPust + (float) gSl + (float) gFuel + tks1 + (float) vEk - (float) gZem);
        edPred67450rab.setTextColor(BLUE);
        edPred67450rab.setText(String.valueOf((int) gPred));
        if (gVzl > 3969) {
            edVes67450rab.setTextColor(RED);
            Toast toast10 = Toast.makeText(getActivity(), "Превышение\nвзлётного\nвеса!!!", Toast.LENGTH_LONG);
            toast10.show();
        }
        /////Далее центровка
        float cG = (((1043.62f + 22.35f + (((float) gFuel / 0.4536f) / 4.92f)
                + 23.90f + 23.90f + (((r1 / 0.4536f) * 192.6f) / 1000f)
                + (((r3 / 0.4536f) * 269.6f) / 1000f)
                + (((a / 0.4536f) * 132.4f) / 1000f) + (((b / 0.4536f) * 182.1f) / 1000f)
                + (((c / 0.4536f) * 233.4f) / 1000f) + (((d / 0.4536f) * 287.6f) / 1000f)
                + (((b4 / 0.4536f) * 294.5f) / 1000f) + (((b6 / 0.4536f) * 344f) / 1000f)
                + (((tks * 9.2f) * 195.77f) / 1000f) - 7.11f) / (gVzl / 0.4536f)) * 1000f);
        float cEntr = (cG - 177.57f) / 0.664f;
        String formattedDouble = new DecimalFormat("#0.00").format(cG);
        edCG67450rab.setText(formattedDouble);//вывод плеча CG location(для графика)
        //edCG67450rab.setText(String.valueOf(cG));//вывод плеча CG location(для графика)
        //edCG67450rab.setText(String.valueOf(Math.floor(cG)));//вывод плеча CG location(для графика)
        edCentr67450rab.setTextColor(BLACK);
        edCentr67450rab.setText(String.valueOf((Math.floor(cEntr * 100f + 0.5f) / 100f)));

        if (cEntr > 40.33) {
            edCentr67450rab.setTextColor(RED);
            Toast toast10 = Toast.makeText(getActivity(), "Предельно\nзадняя\nцентровка!!", Toast.LENGTH_LONG);
            toast10.show();
        }
        if (gVzl <= 3629) {
            float сMin = ((gVzl - 2327.6885245902f) / 54.6769527483f);
            if (cEntr < сMin) {
                edCentr67450rab.setTextColor(RED);
                Toast toast10 = Toast.makeText(getActivity(), "Предельно\nпередняя\nцентровка!!", Toast.LENGTH_LONG);
                toast10.show();
            }
        } else if ((gVzl > 3629) & (gVzl <= 3969)) {
            float cMin = ((gVzl - 2698.8850574713f) / 39.0804597701f);
            if (cEntr < cMin) {
                edCentr67450rab.setTextColor(RED);
                Toast toast10 = Toast.makeText(getActivity(), "Предельно\nпередняя\nцентровка!!", Toast.LENGTH_LONG);
                toast10.show();
            }
        }
    }

}
