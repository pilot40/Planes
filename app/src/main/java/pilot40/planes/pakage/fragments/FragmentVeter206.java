package pilot40.planes.pakage.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.DecimalFormat;

import static java.lang.Math.round;
import static java.lang.Math.sin;

import pilot40.planes.pakage.R;


public class FragmentVeter206 extends Fragment {
    public Spinner spinScep206;
    public Spinner spinVet206;
    public EditText edUv_206, edU_206, edUbok_206, edMs_206, edKph_206;
    public Button btBokRas_206, btMsRas_206;
    //ArrayAdapter<CharSequence> adapspinScep;

    public FragmentVeter206() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_veter_206, container, false);

    }

    @Override
    public void onStart() {
        super.onStart();


        btBokRas_206 = (Button)  getActivity().findViewById(R.id.btBokRas_206);
        btMsRas_206 = (Button)  getActivity().findViewById(R.id.btMsRas_206);

        edUv_206 = (EditText)  getActivity().findViewById(R.id.edUv_206);
        edU_206 = (EditText)  getActivity().findViewById(R.id.edU_206);
        edUbok_206 = (EditText)  getActivity().findViewById(R.id.edUbok_206);
        edMs_206 = (EditText)  getActivity().findViewById(R.id.edMs_206);
        edKph_206 = (EditText)  getActivity().findViewById(R.id.edKph_206);

        spinScep206 = (Spinner) getActivity().findViewById(R.id.spinScep206);
        spinVet206 = (Spinner) getActivity().findViewById(R.id.spinVet206);

        ArrayAdapter<CharSequence> adapspinScep = ArrayAdapter.createFromResource(getActivity(),
                R.array.scep206, android.R.layout.simple_spinner_item);
        adapspinScep.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinScep206.setAdapter(adapspinScep);

        ArrayAdapter<CharSequence> adapspinVet = ArrayAdapter.createFromResource(getActivity(),
                R.array.veter206, android.R.layout.simple_spinner_item);
        adapspinVet.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinVet206.setAdapter(adapspinVet);



        spinScep206.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                int sCep = spinScep206.getSelectedItemPosition();
                spinVet206.setSelection(sCep);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinVet206.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                int vEt = spinVet206.getSelectedItemPosition();
                spinScep206.setSelection(vEt);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btBokRas_206.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edUbok_206.setText("0.0");
                float u = (Float.parseFloat(edU_206.getText().toString()));
                float uv = (Float.parseFloat(edUv_206.getText().toString()));
                float ub = (float)(sin(uv* 3.14159265 / 180))*u;
                String formattedDouble = new DecimalFormat("#0.0").format(ub);
                edUbok_206.setText(formattedDouble);
                //ui->lineUB->setText(QString::number((floor(ub*10+.5)/10)));
            }
        });

        btMsRas_206.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edKph_206.setText("0");
                float vmc = (Float.parseFloat(edMs_206.getText().toString()));
                float vkm = ((vmc*3600)/1000);
                edKph_206.setText(String.valueOf(round(vkm)));//вывод км/ч
            }
        });




    }

}
