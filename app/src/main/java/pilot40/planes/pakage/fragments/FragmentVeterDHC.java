package pilot40.planes.pakage.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.DecimalFormat;

import static java.lang.Math.round;
import static java.lang.Math.sin;

import pilot40.planes.pakage.R;


public class FragmentVeterDHC extends Fragment {
    public Spinner spinScep_dhc;
    public Spinner spinVet_dhc;
    public EditText edUv_DHC, edU_DHC, edUbok_DHC, edMs_DHC, edKph_DHC;
    public Button btBokRas_DHC, btMsRas_DHC;


    public FragmentVeterDHC() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_veter_dhc, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        btBokRas_DHC = (Button)  getActivity().findViewById(R.id.btBokRas_DHC);
        btMsRas_DHC = (Button)  getActivity().findViewById(R.id.btMsRas_DHC);

        edUv_DHC = (EditText)  getActivity().findViewById(R.id.edUv_DHC);
        edU_DHC = (EditText)  getActivity().findViewById(R.id.edU_DHC);
        edUbok_DHC = (EditText)  getActivity().findViewById(R.id.edUbok_DHC);
        edMs_DHC = (EditText)  getActivity().findViewById(R.id.edMs_DHC);
        edKph_DHC = (EditText)  getActivity().findViewById(R.id.edKph_DHC);

        spinScep_dhc = (Spinner) getActivity().findViewById(R.id.spinScep_dhc);
        spinVet_dhc = (Spinner) getActivity().findViewById(R.id.spinVet_dhc);

        ArrayAdapter<CharSequence> adapspinScep_dhc = ArrayAdapter.createFromResource(getActivity(),
                R.array.scep_dhc, android.R.layout.simple_spinner_item);
        adapspinScep_dhc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinScep_dhc.setAdapter(adapspinScep_dhc);

        ArrayAdapter<CharSequence> adapspinVet_dhc = ArrayAdapter.createFromResource(getActivity(),
                R.array.veter_dhc, android.R.layout.simple_spinner_item);
        adapspinVet_dhc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinVet_dhc.setAdapter(adapspinVet_dhc);

        spinScep_dhc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                int sCep = spinScep_dhc.getSelectedItemPosition();
                spinVet_dhc.setSelection(sCep);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinVet_dhc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                int vEt = spinVet_dhc.getSelectedItemPosition();
                spinScep_dhc.setSelection(vEt);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btBokRas_DHC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edUbok_DHC.setText("0.0");
                float u = (Float.parseFloat(edU_DHC.getText().toString()));
                float uv = (Float.parseFloat(edUv_DHC.getText().toString()));
                float ub = (float)(sin(uv* 3.14159265 / 180))*u;
                String formattedDouble = new DecimalFormat("#0.0").format(ub);
                edUbok_DHC.setText(formattedDouble);
                //ui->lineUB->setText(QString::number((floor(ub*10+.5)/10)));
            }
        });

        btMsRas_DHC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edKph_DHC.setText("0");
                float vmc = (Float.parseFloat(edMs_DHC.getText().toString()));
                float vkm = ((vmc*3600)/1000);
                edKph_DHC.setText(String.valueOf(round(vkm)));//вывод км/ч
            }
        });
    }
}
